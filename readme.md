# IOT WITH PYTHON - CANDY FACTORY SENSORS DATA PROCESSING

Read values of various simulated sensors in a candy factory. These values should be communicated over MQTT protocol to the cloud server over a topic. An alert needs to be triggered for sensor data which will be received over MQTT topic depending upon alerting parameters.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

* Basic understanding of MQTT protocol - [REFER](https://docs.google.com/document/d/1leB7TlRjS-sw5dr_MKN1Mp2CEV5dpO3d9pGDk76uPxo/edit?usp=sharing)
* python
* paho-mqtt python's package

### Installing

Clone the repository in your local machine.
Make a virtualenv and activate it.
Run - `pip install -r requirements.txt`

### Setting up the parameters

`settings.py` file contains all the parameters which you need to configure before running. Following is description of each parameter:

1. BROKER_ADDRESS
   - Description - publically-accessible MQTT broker
   - Possible Values - mqtt.flespi.io, iot.eclipse.org, test.mosquitto.org, broker.hivemq.com

2. SENSOR_NAME
   - Description - Name of sensor, values of which you want simulate
   - Possible Values - temperature, humidity, sugar
   - Note - temperature sensor will simulate random values between 22C and 28C. humidity sensor will simulate random values between 50% and 90%. Sugar sensor will simulate random values between 40gm to 90gm.

3. TOPIC_NAME
   - Description - Topic name on which sensor will publish the data and to which cloud server will subscribe it.
   - Possible Values - Whatever you want!

4. TOTAL_TIME
   - Description - Total time for which you want to simulate the sensor values.
   - Possible Values - Any value in seconds

5. TIME_INTERVAL
   - Description - Time interval after which you want sensor values.
   - Possible Values - Any value in seconds (less than TOTAL_TIME)

6. THRESHOLD_LEVEL_UPPER
   - Description - if set to True, Alert will be triggered if average of sensor values for specified THRESHOLD_VALUES_TIME becomes More than AVG_THRESHOLD. If False, Alert will be triggered if average of sensor values for specified THRESHOLD_VALUES_TIME becomes Less than AVG_THRESHOLD
   - Possible Values - True or False

7. AVG_THRESHOLD
   - Description - Average Threshold value of sensor readings during the time specified in 'THRESHOLD_VALUES_TIME' for which alert should get generated.
   - Possible Values - Value depending upon the sensor selected. Note the following before setting up this value:
     - Temperature Sensor will generate values between 22C to 28C.
     - Humidity Sensor will generate values between 50% to 90%.
     - Sugar Sensor will generate values between 40gm to 90gm.

8. THRESHOLD_VALUES_TIME
   - Description - Time for which you want to calculate the average of received values on cloud_server.
   - Possible Values - Any value in seconds (Less than TOTAL_TIME)

### Sample Test Scenarios

1. Simulate to send humidity sensor values to a pre-defined topic "sensor_data" at 5 seconds interval for total time of 2 minutes. The alerting module script should receive data over a topic and then raise an alert if average of humidity values received is more than 80% for more than 1 minutes. You need to set following parameters in settings.py to simulate this situation:

   - BROKER_ADDRESS - Any of your choice
   - SENSOR_NAME - 'humidity'
   - TOPIC_NAME - 'sensor_data'
   - TOTAL_TIME - 120
   - TIME_INTERVAL - 5
   - THRESHOLD_LEVEL_UPPER - True
   - AVG_THRESHOLD - 80
   - THRESHOLD_VALUES_TIME - 60

2. Simulate to send Temerature sensor values to a pre-defined topic "sensor_data" at 2 seconds interval for total time of 1 minute. Alert should be raised if average of temerature values received is less than 25C for 10 seconds. You need to set following parameters in settings.py to simulate this situation:

   - BROKER_ADDRESS - Any of your choice
   - SENSOR_NAME - 'temperature'
   - TOPIC_NAME - 'sensor_data'
   - TOTAL_TIME - 60
   - TIME_INTERVAL - 2
   - THRESHOLD_LEVEL_UPPER - False
   - AVG_THRESHOLD - 25
   - THRESHOLD_VALUES_TIME - 10

## Run
After installation and setting up the parameters depending upon the test scenario, run the following in the command line.
`python main.py`

## Authors

* **Raghav Chopra**

## Acknowledgments

* Zenatix Solutions Pvt. Ltd.

