#Settings for Temperature Sensor

BROKER_ADDRESS="iot.eclipse.org" #mqtt-broker

SENSOR_NAME = 'temperature' #Possible values - temperature, humidity, sugar

TOPIC_NAME = "sensor_data"	#topic Name to which sensor and cloud server will subscribe

TOTAL_TIME = 60	#total time(in sec) for which you want sensor readings

TIME_INTERVAL = 2	#Time interval (in sec) after which you want sensor values

THRESHOLD_LEVEL_UPPER = False #if True, alert will be triggered if average of sensor values for specified THRESHOLD_VALUES_TIME becomes More than AVG_THRESHOLD
							 #if False, alert will be triggered if average of sensor values for specified THRESHOLD_VALUES_TIME becomes Less than AVG_THRESHOLD 

AVG_THRESHOLD = 25	#Average Threshold value of sensor readings during the time specified in THRESHOLD_VALUES_TIME

THRESHOLD_VALUES_TIME = 10	#Time(in sec) for which you want to calculate the average of received values on cloud_server


