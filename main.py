'''
@author: raghav
'''
import time
import paho.mqtt.client as mqtt
from settings import BROKER_ADDRESS, SENSOR_NAME, TOPIC_NAME, TOTAL_TIME, TIME_INTERVAL,\
							 AVG_THRESHOLD,THRESHOLD_VALUES_TIME
import random
from alert import check_alert

received_values = []
def on_message(client, userdata, message):
	
	global received_values

	time.sleep(1)
	value = str(message.payload.decode("utf-8"))
	value_time = time.time()
	print("Value Received by Cloud Server --> ",value)
	received_values.append([value,value_time])
	check_values = []

	for i in range(len(received_values)-1,0,-1):
		if(value_time-received_values[i][1]>THRESHOLD_VALUES_TIME):
			check_values = [x for x,y in received_values[i:]]
			check_alert(check_values,AVG_THRESHOLD)
			break

def main():

	print("clientA creating new instance")
	clientA = mqtt.Client(SENSOR_NAME)

	print("clientB creating new instance")
	clientB = mqtt.Client('cloud_server')

	clientB.on_message=on_message

	print("clientA connecting to broker")
	clientA.connect(BROKER_ADDRESS, port=1883)

	print("clientB connecting to broker")
	clientB.connect(BROKER_ADDRESS, port=1883)
	time.sleep(2)

	print("ClientB Subscribing to topic",TOPIC_NAME)
	clientB.subscribe(TOPIC_NAME)
	time.sleep(3)

	t_end = time.time() + TOTAL_TIME

	clientB.loop_start()
	t_start = time.time()
	if SENSOR_NAME.lower()=='temperature':
		while(time.time() < t_end):
			print("Publishing temperature value to the topic",TOPIC_NAME)
			clientA.publish(TOPIC_NAME,random.randint(22,28))
			time.sleep(TIME_INTERVAL)

	elif SENSOR_NAME.lower()=='humidity':
		while(time.time() < t_end):
			print("Publishing humidity value to the topic",TOPIC_NAME)
			clientA.publish(TOPIC_NAME,random.randint(50,90))
			time.sleep(TIME_INTERVAL)

	elif SENSOR_NAME.lower()=='sugar':
		while(time.time() < t_end):
			print("Publishing sugar values to the topic",TOPIC_NAME)
			clientA.publish(TOPIC_NAME,random.randint(40,90))
			time.sleep(TIME_INTERVAL)

	else:
		print("##### PLEASE SET CORRECT VALUE OF SENSOR in settings.py ######")

	clientB.disconnect()
	clientB.loop_stop()

	print("ALL RECEIVED VALUES --> ",received_values)

if __name__=='__main__':
	main()