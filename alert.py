'''
author: @raghav
'''
from settings import THRESHOLD_LEVEL_UPPER

def check_alert(values_list,avg_threshold):
	'''
	Used to generate Alert
	@Input params:
		values_list: list, values for which we need to find average
	'''
	global THRESHOLD_LEVEL_UPPER

	print("NOW CHECKING FOR ALERT for values-->", values_list)
	if len(values_list)==0:
		return 0

	values_count = len(values_list)
	values_sum = 0
	for v in values_list:
		values_sum = float(values_sum) + float(v)

	avg = float(values_sum)/values_count
	print("Average is -->", avg)
	
	if THRESHOLD_LEVEL_UPPER is True:
		if avg>=avg_threshold:
			print("##########################################")
			print("####### ALERT INITIATED ##################")
			print("####### UNEXPECTED VALUES RECEIVED #######")
			print("##########################################")
		else:
			print("####### No Alert #########################")
	elif THRESHOLD_LEVEL_UPPER is False:
		if avg<avg_threshold:
			print("##########################################")
			print("####### ALERT INITIATED ##################")
			print("####### UNEXPECTED VALUES RECEIVED #######")
			print("##########################################")
		else:
			print("####### No Alert #########################")

